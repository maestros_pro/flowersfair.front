import '../css/style.scss'

import $ from 'jquery'
import './modules/sl'
import ViewPort from './modules/module.viewport'
import Tab from './modules/module.tab'
import noUiSlider from 'nouislider'
import './modules/module.form'
import './modules/module.helper'
import Share from './modules/module.share'
import Popup from './modules/module.popup'
import Plyr from 'plyr/dist/plyr.polyfilled.js'
import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min'

window.app = window.app || {};

$(()=>{

	let $b = $('body'), app, breakpoint, player;

	if ( $('#previewPlayer').length ){
		player = new Plyr('#previewPlayer', {
			debug: isLocal,
			autoplay: true,
			playerVars:{
				controls: 0,
				rel: 0
			},
			controls: ['play-large', 'play', 'progress', 'current-time', 'mute', 'volume', 'captions', 'pip', 'airplay', 'fullscreen']
		});

		player.on('ready', (e)=>{
			$(e.target).addClass('is-ready');
		})
	}

	new Share();

	let popup = new Popup();

	new Tab({
		classActiveLink: 'is-active',
		classActiveTab: 'is-active',
		onTabChange: function () {

		}
	});

	function basketCounter($product, val){
		let $counter = $product.find('.cart__product-counter'),
			price = $product.attr('data-price'),
			max = parseInt($counter.attr('data-count-max'))
		;

		if ( val < 1 ) val = 1;
		if ( val > max ) val = max;

		$product.find('.cart__product-counter-value').html(val);
		$product.find('.cart__product-price-actual').find('span').html((price * val).discharge());

		if (val > 1){
			$product.find('.cart__product-price-calc').html(`<span>${val}</span> х ${(price).discharge()} &#x20bd;`); //&#8381;
		} else {
			$product.find('.cart__product-price-calc').html('');
		}

		$counter.removeClass('is-open');
		$product.find('.cart__product-counter-item').removeClass('is-active');
		$product.find('.cart__product-counter-item').eq(val - 1).addClass('is-active');


		if ( window.app.productBasketChange && typeof window.app.productBasketChange === 'function') window.app.productBasketChange($product.attr('data-id'), val);
	}

	$b
		.on('mousedown', (e)=>{
			if ( $('.header_search').length ){
				if ( !$(e.target).closest('.header__search').length ) $('.header_search').removeClass('header_search');
			}
			if ( $('.cart__product-counter.is-open').length ){
				if ( !$(e.target).closest('.cart__product-counter.is-open').length ) $('.cart__product-counter').removeClass('is-open');
			}
		})
		.on('click', '.header__handler', ()=>{
			$b.addClass('is-nav-show');
		})

		.on('click', '.cart__product-counter-btn', function(){
			let $t = $(this),
				$parent = $t.closest('.cart__product-counter'),
				val = parseInt($parent.find('.cart__product-counter-value').html()),
				more = $t.hasClass('cart__product-counter-btn_more'),
				less = $t.hasClass('cart__product-counter-btn_less')
			;

			if ( less ){
				val--
			} else if ( more ){
				val++
			}

			basketCounter($t.closest('.cart__product-item'), val);

		})
		.on('click', '.cart__product-counter', function(){
			$(this).addClass('is-open')
		})
		.on('mousedown', '.cart__product-counter-item', function(){
			let $t = $(this),
				val = $t.attr('data-value')
			;
			basketCounter($t.closest('.cart__product-item'), val);
		})

		.on('click', '.cart__product-delete', function(){
			let $t = $(this),
				$product = $t.closest('.cart__product-item')
			;

			if ( window.app.productBasketRemove && typeof window.app.productBasketRemove === 'function') window.app.productBasketRemove($product.attr('data-id'));

			$product.fadeTo(200, 0).slideUp(300, ()=>{
				$product.remove()
			});
		})


		.on('click', '.nav__overlay', ()=>{
			$b.removeClass('is-nav-show');
			$('.nav__item_active').removeClass('nav__item_active');
		})
		.on('click', '.catalog__head-filter', ()=>{
			$b.addClass('is-filter-show');
		})
		.on('keyup', '[data-regex]', function(e){

			let $t = $(this),
				$form = $t.closest('form'),
				regex = new RegExp($t.attr('data-regex'));

			if ( regex.test(e.target.value) ){
				$form.find('.btn').prop('disabled', false);
			} else {
				$form.find('.btn').prop('disabled', true);
			}
		})
		.on('click', '.catalog__filter-overlay, .catalog__filter-close', ()=>{
			$b.removeClass('is-filter-show');
		})
		.on('click', '.nav__item', function(e){
			let $t = $(this),
				isBack = $t.closest('.nav__submenu-back').length,
				isSubmenu = $t.find('.nav__submenu').length
			;

			if ( isSubmenu  ){
				$t.addClass('nav__item_active');
				e.stopPropagation();
			} else if ( isBack){
				$t.closest('.nav__item_active').removeClass('nav__item_active');
				e.stopPropagation();
			}

		})
		.on('click', '.header__search-ico', function(){
			let $t = $(this),
				$header = $('.header'),
				$wrap = $t.closest('.header__search')
			;

			if ( $header.hasClass('header_search') ) {
				$t.closest('form').submit();
				return false;
			}

			$header.addClass('header_search');
			$wrap.find('input').focus();
		})
		.on('click', '.header__search-result .all', function(){
			$(this).closest('form').submit();
		})
		.on('click', '[data-scroll]', function(e){
			e.preventDefault();
			let offset = breakpoint === 'mobile' ? 140 : 20;
			$('html,body').animate({scrollTop: $($(this).attr('data-scroll')).offset().top - offset}, 500);
		})
		.on('click', '.footer__scrolltop', function(e){
			$('html,body').animate({scrollTop: 0}, 500);
		})
		.on('click', '.catalog__filter-title', function(){
			let $t = $(this),
				$group = $t.siblings('.catalog__filter-group')
			;

			if ( !$t.hasClass('is-buzy') ){
				$t.addClass('is-buzy');

				$t.toggleClass('is-open');
				$group.slideToggle(300, ()=>{
					$group.toggleClass('is-open').removeAttr('style');
					$t.removeClass('is-buzy');
				})
			}
		})
		.on('click', '.auth__order-title_drop', function(){
			let $t = $(this),
				$group = $t.siblings('.auth__order-inner')
			;

			if ( !$t.hasClass('is-buzy') && breakpoint === 'mobile'){
				$t.addClass('is-buzy');

				$t.toggleClass('is-open');
				$group.slideToggle(300, ()=>{
					$group.toggleClass('is-open').removeAttr('style');
					$t.removeClass('is-buzy');
				})
			}
		})
		.on('change', '.product__label-item input', function(){
			let $t = $(this),
				$group = $t.closest('.product__label-group'),
				$val = $group.find('.product__label-title').find('b'),
				val = $t.siblings('.product__label-inner').html()
			;

			$val.html(val);
		})

		.on('change', '[data-show]', (e)=>{
			let $input = $(e.target),
				box = $input.attr('data-show')
			;

			if (e.target.type === 'checkbox') {

				if ( e.target.checked ){
					$(box).removeClass('is-hidden')
				} else{
					$(box).addClass('is-hidden')
				}

			} else if (e.target.type === 'radio') {
				$(`input[name = "${e.target.name}"]`).each(function(){
					let $input = $(this);
					$($input.attr('data-show')).addClass('is-hidden');
				});

				$(box).removeClass('is-hidden');
			}

		})

		.on('click', '.feedback__item-more a', function(e){
			e.preventDefault();
			$(this).closest('.feedback__item').find('.feedback__item-hide').removeClass('feedback__item-hide');
			$(this).closest('.feedback__item-more').addClass('is-hidden');
		})
	;


	let $st = $('.footer__scrolltop');

	$(window).on('scroll', (e)=>{

		if ( $(window).scrollTop() > $(window).height() && !$st.hasClass('is-active')){
			$st.addClass('is-active');
		} else if ( $(window).scrollTop() <= $(window).height() && $st.hasClass('is-active')){
			$st.removeClass('is-active');
		}
	});

	if ( window.ymaps ){
		ymaps.ready(function(){



			$('.contacts__map').each(function () {
				let $t = $(this);

				let map = new ymaps.Map($t[0],{
					center: [60.079882, 30.367144],
					zoom: 15,
					controls: []
				});

				/*map.controls.add('zoomControl', {
					size: 'small',
					float: 'none',
					position: {
						top: '250px',
						left: '30px'
					}
				});*/


				let myPlacemark = new ymaps.Placemark([60.079882, 30.367144],
					{},
					{
						iconLayout: 'default#image',
						iconImageHref: '/img/svg/marker.svg'
					});
				map.geoObjects.add(myPlacemark);

				map.behaviors.disable('scrollZoom');
				map.behaviors.disable('multiTouch');
				map.behaviors.disable('drag');


				new ViewPort({
					'0': ()=>{
						map.container.fitToViewport();

						myPlacemark.options.set({
							iconImageSize: [79, 103],
							iconImageOffset: [-40, -103]
						});
					},
					'1200': ()=>{
						map.container.fitToViewport();

						myPlacemark.options.set({
							iconImageSize: [43, 56],
							iconImageOffset: [-22, -56]
						});
					}
				});

			});


		});
	}





	window.app.init= ()=>{


		$('.scrollbar').each(function(){
			let $t = $(this);
			if ($t.hasClass('is-inited')) return false;
			$t.addClass('is-inited');

			$t.mCustomScrollbar();

		});

		$('.poster__list').each(function(){
			let $t = $(this);

			if ($t.hasClass('is-inited')) return false;
			$t.addClass('is-inited');

			$t.slick({
				infinite: true,
				speed: 300,
				swipe: true,
				swipeToSlide: true,
				arrows: true,
				dots: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				adaptiveHeight: true,
				fade: false,
				prevArrow: '<div class="slick-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50"><path d="M63.97,458l1.489-1.5-3.439-3.459H77v-2.114H62.04l3.421-3.446-1.491-1.5L58,451.994Z" transform="translate(-42.5 -427)"/></svg></div>',
				nextArrow: '<div class="slick-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50"><path d="M71.03,446l-1.489,1.5,3.439,3.459H58v2.114H72.96l-3.421,3.446,1.491,1.5L77,452.006Z" transform="translate(-42.5 -427)"/></svg></div>',
			});
		});

		$('.card_carousel').each(function(){
			let $t = $(this),
				$slider = $t.find('.card__list'),
				five = $t.hasClass('card_five'),
				six = $t.hasClass('card_six'),
				dots = $t.hasClass('card_dots')
			;

			if ( breakpoint === 'mobile' ){

				if ($slider.hasClass('is-inited')) {
					$slider.removeClass('is-inited');
					$slider.slick('unslick');
				}
			} else if ( !$slider.hasClass('is-inited') ){
				$slider.addClass('is-inited');

				$slider.slick({
					infinite: true,
					speed: 300,
					swipe: true,
					swipeToSlide: true,
					arrows: true,
					dots: dots,
					slidesToShow: five ? 5 : six ? 6 : 4,
					slidesToScroll: 1,
					fade: false,
					prevArrow: '<div class="slick-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50"><path d="M63.97,458l1.489-1.5-3.439-3.459H77v-2.114H62.04l3.421-3.446-1.491-1.5L58,451.994Z" transform="translate(-42.5 -427)"/></svg></div>',
					nextArrow: '<div class="slick-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50"><path d="M71.03,446l-1.489,1.5,3.439,3.459H58v2.114H72.96l-3.421,3.446,1.491,1.5L77,452.006Z" transform="translate(-42.5 -427)"/></svg></div>',
					variableWidth: false
				});
			}

		});


		$('.feedback__slider').each(function(){
			let $t = $(this);

			if ($t.hasClass('is-inited')) return false;
			$t.addClass('is-inited');

			$t.slick({
				infinite: true,
				speed: 300,
				swipe: true,
				swipeToSlide: true,
				arrows: true,
				dots: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				fade: false,
				prevArrow: '<div class="slick-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50"><path d="M63.97,458l1.489-1.5-3.439-3.459H77v-2.114H62.04l3.421-3.446-1.491-1.5L58,451.994Z" transform="translate(-42.5 -427)"/></svg></div>',
				nextArrow: '<div class="slick-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50"><path d="M71.03,446l-1.489,1.5,3.439,3.459H58v2.114H72.96l-3.421,3.446,1.491,1.5L77,452.006Z" transform="translate(-42.5 -427)"/></svg></div>',
				adaptiveHeight: true,
				responsive: [
					{
						breakpoint: 1200,
						settings: {
							arrows: false,
							dots: true,
						}
					}
				]
			});


		});

		$('.product__image-preview-list').each(function(){
			let $t = $(this),
				$view = $('.product__image-view')
			;

			if ($t.hasClass('is-inited')) return false;
			$t.addClass('is-inited');

			$view.css({backgroundImage: `url(${$t.find('.product__image-preview-item').eq(0).addClass('is-active').find('[data-image]').attr('data-image')})`})


			$t.on('click', '.product__image-preview-item', function(){
				$t.find('.product__image-preview-item').removeClass('is-active');
				$view.removeClass('product__image-view_video');

				$view.removeAttr('style');

				let $this = $(this),
					$img = $this.find('.product__image-preview-img')
				;

				try { player.stop(); } catch (e){}

				$this.addClass('is-active');



				if ( $img.attr('data-video') ){
					$view.css({
						backgroundImage: 'url(/img/loading.svg)',
						backgroundColor: '#000',
						backgroundSize: '100px 100px',
					});

					let sources = [],
						type = $img.attr('data-video'),
						src = $img.attr('data-src')
					;

					console.info(type, src);

					if ( type === 'html5'){

						let name = $img.attr('[data-src]').split('.');
						sources = [
							{
								src,
								type: `video/${name[name.length - 1]}`
							}
						]

					} else {

						sources = [
							{
								src,
								provider: type,
							},
						]
					}

					player.source = {
						type: 'video',
						title: '',
						sources
					};

					$view.addClass('product__image-view_video');
				} else {
					$view.css({backgroundImage: `url(${$img.attr('data-image')})`})
				}




			})


		});






		$('.form__range').each(function () {
			let $range = $(this),
				$line = $range.find('.form__range-line'),
				values = [],
				min = +$range.attr('data-value-min'),
				max = +$range.attr('data-value-max'),
				pmin = $range.attr('data-value-pmin') ? +$range.attr('data-value-pmin') : min ,
				pmax = $range.attr('data-value-pmax') ? +$range.attr('data-value-pmax') : max,

				$radio = $range.closest('.catalog__filter-group').find('[data-limit]')
			;

			if ($range.hasClass('is-inited')) return false;
			$range.addClass('is-inited');

			if (min === 'NaN' || max === 'NaN' || min === 'undefined' || max === 'undefined' ){
				console.error('Добавьте минимальное и максимаьлное значение фильтра');
				return false;
			}

			let slider = noUiSlider.create($line[0], {
				start: [pmin, pmax],
				connect: true,
				step: 1,
				//margin: max - min > 1000 ? 100 : max - min > 100 ? 10 : 1,
				range: {
					'min': min,
					'max': max
				}
			});

			values[0] = pmin;
			values[1] = pmax;

			$radio.on('change', function(){
				let $t = $(this),
					range = $t.attr('data-limit').split(',')
				;

				$line[0].noUiSlider.set([range[0], range[1] || Infinity]);
			});

			$line[0].noUiSlider.on('end', ()=>{
				$radio.prop('checked', false);
			});

			$line[0].noUiSlider.on('update', (val)=>{
				$range.find('.form__range-value-item_min').find('.form__range-value-input').val(+val[0]);
				$range.find('.form__range-value-item_max').find('.form__range-value-input').val(+val[1]);

				$range.find('.form__range-line').find('.noUi-handle-lower').html(`<span>${(+val[0]).discharge()}</span>`);
				$range.find('.form__range-line').find('.noUi-handle-upper').html(`<span>${(+val[1]).discharge()}</span>`);

			});


			$line[0].noUiSlider.on('set', (val)=>{
				if ( +values[0] !== +val[0] || +values[1] !== +val[1] ) {
					if ( window.app.sliderChange && typeof window.app.sliderChange === 'function') window.app.sliderChange(val[0], val[1]);
				}

				values[0] = val[0];
				values[1] = val[1];
			});

			$range
				.on('keyup', '.form__range-value-input', function () {

					let $t = $(this);

					if ( $t.val().match(/[^0-9]/g) ) {
						let _newVal = $t.val().replace(/[^0-9\.]/g, '') || 0;
						$t.val(_newVal);
					}

				})
				.on('change', '.form__range-value-input', function () {
					$line[0].noUiSlider.set([+$range.find('.form__range-value-item_min').find('.form__range-value-input').val(), +$range.find('.form__range-value-item_max').find('.form__range-value-input').val()]);
				})
			;


		});

	};


	window.app.popup = popup;
	window.app.breakpoint = breakpoint;


	new ViewPort({
		'0': ()=>{
			breakpoint = 'mobile';
			window.app.init();
		},
		'1200': ()=>{
			breakpoint = 'desktop';
			window.app.init();
		}
	});

	$(window).trigger('resize');
});
