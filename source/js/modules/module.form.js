import $ from 'jquery'
import Form from './module.validate'
import Inputmask from 'inputmask'
import 'air-datepicker'

window.app = window.app || {};

$(()=>{


	/*let data = [
		{
			name: 'password',
			error: true,
			success: true,
			message: 'фвфывфы'
		}
	];*/

	let scrollTimer = null;

	window.app.formResult = (data)=>{

		for (let i = 0; i < data.length; i ++){

			let $input = $(`[name = "${data[i].name}"]`),
				$field = $input.closest('.form__field'),
				$message = $field.find('.form__message')
			;

			if (!$message.length){
				$message = $('<div class="form__message"></div>').appendTo($field.eq($field.length - 1));
			}

			if (data[i].error) $field.addClass('f-error');
			if (data[i].success) $field.addClass('f-success');
			if (data[i].message) {
				$field.addClass('f-message');
				$message.html(data[i].message)
			}
		}

		if ($('.f-error').length) {

			clearTimeout(scrollTimer);
			scrollTimer = setTimeout(()=>{
				let offset = window.app.breakpoint === 'mobile' ? 140 : 20;
				$('html,body').animate({scrollTop: $('.f-error').eq(0).offset().top - offset}, 500);

			}, 200);

		}

	};


	const form = new Form();


	$('input[data-mask]').each(function () {
		let $t = $(this),
			inputmask = new Inputmask({
				mask: $t.attr('data-mask'),		//'+7 (999) 999-99-99',
				showMaskOnHover: false,
				onincomplete: function() {
					this.value = '';
					$t.closest('.form__field').removeClass('f-filled');
				}
			});
		inputmask.mask($t[0]);
	});

	$('.form__input_date input').each(function () {
		let $t = $(this),
			datepicker = $t.datepicker({
				minDate: new Date(),
				onSelect:()=>{
					$t.trigger('change');
					if (window.app.changeDate && typeof window.app.changeDate === 'function' ) window.app.changeDate($t.attr('name'), $t.val());
				}
			});
	});


	$('body')
		.on('click', '.form__input-clear', function (e) {
			let $t = $(this),
				$field = $t.closest('.form__field'),
				$input = $field.find('input')
			;
			$input.val('');
			$field.removeClass('f-filled f-error f-message')

		})
		.on('click', '.form__input-arrow', function (e) {
			let $t = $(this),
				$field = $t.closest('.form__field'),
				$input = $field.find('input')
			;
			$input.focus();

		})
		.on('mousedown', '.form__select-item', function (e) {
			let $t = $(this),
				$inputText = $t.closest('.form__field').find('.form__input-text'),
				$inputValue = $t.closest('.form__field').find('.form__input-value'),
				value = $t.attr('data-value'),
				text = $t.html()
			;

			$inputText.val(text).trigger('change');
			$inputValue.val(value).trigger('change');

			if (window.app.changeSort && typeof window.app.changeSort === 'function' ) window.app.changeSort($inputValue.attr('name'), value, text);
		})

		.on('blur', '.form__field input, .form__field textarea', function (e) {
			let $input = $(this),
				$field = $input.closest('.form__field');
			$field.removeClass('f-focused');

			if ( !$.trim($input.val()) ){
				$field.removeClass('f-filled');
			} else {
				$field.addClass('f-filled');
			}
		})

		.on('change', '[data-require]', function (e) {
			let $t = $(this),
				$input = $(`[name = "${$t[0].name}"]`),
				$field = $input.closest('.form__field');

			$field.removeClass('f-error f-message f-success');

		})

		.on('submit', '.form', function (e) {
			let $form = $(this),
				$item = $form.find('input, textarea'),
				wrong = false
			;

			$item
				.closest('.form__field')
				.removeClass('f-error f-message f-success')
				.find('.form__message')
				.html('');

			$item.filter(function(){return $(this).is(':visible')}).each(function () {
				let input = $(this), rule = $(this).attr('data-require');

				if ( rule ){
					form.validate(input[0], rule, err =>{
						if (err.errors.length) {
							wrong = true;
							let data = [{
								name: input[0].name,
								error: !!err.errors.length,
								message: err.errors[0]
							}];

							window.app.formResult(data);

							/*$(input)
								.closest('.form__field')
								.addClass('f-error f-message')
								.find('.form__message')
								.html(err.errors[0])
							;*/
						}
					})
				}
			});
			if ( wrong ){
				e.preventDefault();
			} else {
				e.preventDefault();
				$form[0].dispatchEvent(new Event('formSubmit'));
			}

		});

});